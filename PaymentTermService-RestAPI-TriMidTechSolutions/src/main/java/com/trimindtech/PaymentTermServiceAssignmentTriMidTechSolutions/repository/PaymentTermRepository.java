package com.trimindtech.PaymentTermServiceAssignmentTriMidTechSolutions.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.trimindtech.PaymentTermServiceAssignmentTriMidTechSolutions.entity.PaymentTermEntity;

public interface PaymentTermRepository extends JpaRepository<PaymentTermEntity, Serializable> {
	
}
