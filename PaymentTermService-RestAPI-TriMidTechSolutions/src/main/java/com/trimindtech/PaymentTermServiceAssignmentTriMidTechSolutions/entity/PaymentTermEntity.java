package com.trimindtech.PaymentTermServiceAssignmentTriMidTechSolutions.entity;

import java.sql.Date;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

@Entity
@Data
@XmlRootElement
@Table(name = "Payment_Term_Entity")
public class PaymentTermEntity {
	
	@GeneratedValue
	@Column(name = "id")
	@Id
	private Integer id;
	
	@Column(name = "code")
	private String code;
	
	@Column(name = "description")
	private String description;
	
	@Column(name = "creationDate")
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="dd-MM-yyyy")
	private Date creationDate;
	
	@Column(name = "days")
	private Integer days;
	
	@Column(name = "reminderBeforeDays")
	private String reminderBeforeDays;
}
