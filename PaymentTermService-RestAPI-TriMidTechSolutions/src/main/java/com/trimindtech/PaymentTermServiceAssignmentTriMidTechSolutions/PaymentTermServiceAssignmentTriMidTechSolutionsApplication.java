package com.trimindtech.PaymentTermServiceAssignmentTriMidTechSolutions;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PaymentTermServiceAssignmentTriMidTechSolutionsApplication {

	public static void main(String[] args) {
		SpringApplication.run(PaymentTermServiceAssignmentTriMidTechSolutionsApplication.class, args);
	}

}
