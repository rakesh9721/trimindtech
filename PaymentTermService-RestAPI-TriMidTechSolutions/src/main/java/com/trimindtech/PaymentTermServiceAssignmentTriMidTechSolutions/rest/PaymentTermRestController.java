package com.trimindtech.PaymentTermServiceAssignmentTriMidTechSolutions.rest;

import java.util.List;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.trimindtech.PaymentTermServiceAssignmentTriMidTechSolutions.entity.PaymentTermEntity;
import com.trimindtech.PaymentTermServiceAssignmentTriMidTechSolutions.repository.PaymentTermRepository;


@RestController
public class PaymentTermRestController {
	
	@Autowired
	private PaymentTermRepository paymentTermRepository;
	
	@PostMapping(value = "/save",consumes = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<String> addRecord(@RequestBody PaymentTermEntity paymentTermEntity){
		paymentTermRepository.save(paymentTermEntity);
		String responseMsg="record inserted successfully";
		return new ResponseEntity<String>(responseMsg, HttpStatus.CREATED);
	}
	
	@GetMapping(value = "/getpaymenttermrecordbyid/{id}",produces = {MediaType.APPLICATION_JSON_VALUE,
																   MediaType.APPLICATION_XML_VALUE})
	public ResponseEntity<PaymentTermEntity> getPaymentTermRecordById(@PathVariable("id") Integer id){
		PaymentTermEntity fetchPaymentTermRecord=null;
		Optional<PaymentTermEntity> findPaymentTerRecordById = paymentTermRepository.findById(id);
		if(findPaymentTerRecordById.isPresent())
			fetchPaymentTermRecord=findPaymentTerRecordById.get();
		return new ResponseEntity<PaymentTermEntity>(fetchPaymentTermRecord, HttpStatus.OK);
	}
	
	@GetMapping(value = "/getallpaymenttermrecord",produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<PaymentTermEntity>> getAllPaymentTermRecord(){
		List<PaymentTermEntity> findAllPaymentTermRecords = paymentTermRepository.findAll();
		return new ResponseEntity<List<PaymentTermEntity>>(findAllPaymentTermRecords,HttpStatus.OK);
	}
	
	@PutMapping(value = "/updatepaymenttermrecord/{id}",consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> updatePaymentTermRecord(@RequestBody PaymentTermEntity paymentTermEntity, @PathVariable Integer id){
		Optional<PaymentTermEntity> findRecordById = paymentTermRepository.findById(id);
		if(!findRecordById.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		paymentTermEntity.setId(id);
		paymentTermRepository.save(paymentTermEntity);
		String msg="record updated successfully";
		return new ResponseEntity<String>(msg,HttpStatus.OK);
	}
	
	@DeleteMapping(value = "/deletepaymenttermrecord/{id}")
	public ResponseEntity<String> deletePaymentTermRecord(@PathVariable("id") Integer id){
		paymentTermRepository.deleteById(id);
		String msg="record deleted successfully";
		return new ResponseEntity<String>(msg, HttpStatus.OK);
	}
}
